import 'package:flutter/material.dart';
import 'package:provider_test/model/pet.dart';
import 'package:provider_test/provider/default_pet_provider.dart';

class PetsProvider with ChangeNotifier implements DefaultPetProvider {
  List<Pet> _pets = [];

  List<Pet> get pets => _pets;

  set pets(List<Pet> value) {
    _pets = value;
    notifyListeners();
  }
}